import { Component, OnInit } from '@angular/core';
import { ApiClientService } from '../../../shared/services/api-service/api-client.service';
import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor(private apiService: ApiClientService,
              private userService: UserService) {}
  /*
  * logout and emit removeToken event
  */
  logout() {
    this.userService.removeToken();
  }

}
