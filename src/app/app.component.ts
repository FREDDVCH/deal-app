import { Component } from '@angular/core';
import { UserService } from './core/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private userService: UserService,
              private router: Router) {
    this.userService.authenticated
      .subscribe((status) => this.changeUserStatusHandler(status));
  }

  /*
  * navigate to login if token cleared or expired
  * @param {boolean} status
  */
  changeUserStatusHandler(status): void {
    if (!status) {
      this.router.navigate(['/login']);
    }
  }
}
