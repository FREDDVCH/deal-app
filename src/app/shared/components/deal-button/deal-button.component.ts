import { Component, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-deal-button',
  templateUrl: './deal-button.component.html',
  styleUrls: ['./deal-button.component.scss']
})
export class DealButtonComponent implements OnInit {

  @Input() disableState: boolean;
  @Input() classString: string;
  @Input() label: string;
  @Output() clickEvent: Subject<any> = new Subject<any>();

  constructor() { }

  ngOnInit() {
  }

  /*
  * click event emit
  */
  clickButton(): void {
    this.clickEvent.next(true);
  }

}
