import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealButtonComponent } from './deal-button.component';

describe('DealButtonComponent', () => {
  let component: DealButtonComponent;
  let fixture: ComponentFixture<DealButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
