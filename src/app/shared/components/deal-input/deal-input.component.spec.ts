import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealInputComponent } from './deal-input.component';

describe('DealInputComponent', () => {
  let component: DealInputComponent;
  let fixture: ComponentFixture<DealInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
