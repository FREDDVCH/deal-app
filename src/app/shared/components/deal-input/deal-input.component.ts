import {
  Component,
  ContentChild,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ErrorMessage } from '../../models/error-message';
import { ErrorMessageFactory } from '../../utils/error-message-factory';

import { SubSink } from 'subsink';
import {
  debounceTime,
  distinctUntilChanged } from 'rxjs/internal/operators';

@Component({
  selector: 'app-deal-input',
  templateUrl: './deal-input.component.html',
  styleUrls: ['./deal-input.component.scss']
})
export class DealInputComponent implements OnInit, OnDestroy {
  @Input() inputId: string;
  @Input() inputType: string;
  @Input() classString: string;
  @Input() formObject: FormControl;

  @ContentChild('label', { static: true }) label: TemplateRef<any>;
  @ContentChild('button', { static: true }) button: TemplateRef<any>;
  @ViewChild('inputElement', { static: true }) inputElement: ElementRef;

  errorObj: ErrorMessage;
  subs: SubSink = new SubSink();

  constructor() { }

  ngOnInit() {
    this.subs.add(this.formObject
      .valueChanges
      .pipe(
        debounceTime(200),
        distinctUntilChanged()
      )
      .subscribe( () => {
        if (!this.formObject.valid) {
          this.formObject.markAsPending();
          this.formObject.markAsTouched();
        }
        this.setErrorMessage();
      }));
  }

  /*
  * set error object to view
  */
  setErrorMessage() {
    if (this.formObject.errors) {
      this.errorObj = new ErrorMessageFactory(this.formObject.errors).getErrorMessage();

      return;
    }
    this.errorObj = null;
  }

  /*
  * set focus on input
  */
  setFocus() {
    this.inputElement.nativeElement.focus();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
