import { ErrorMessage } from '../models/error-message';

/*
* error factory for create an error whit translated title and value (for value types)
*/
export class ErrorMessageFactory {

  constructor(formError: any) {
    this.formError = formError;
    switch (this.getErrorType(this.formError)) {
      case 'minlength':
      case 'minValue':
      case 'maxlength':
      case 'maxValue':
        this.error = new ErrorMessage(this.getErrorType(this.formError), this.getErrorRequiredValue(this.formError));
        break;
      default:
        this.error = new ErrorMessage(this.getErrorType(this.formError));
        break;
    }

  }

  getErrorType(obj: object, prfx: string = '') {
    if (typeof obj === 'object' && obj) {
      return Object.keys(obj)[0] + prfx;
    }
  }

  getErrorRequiredValue(obj: object) {
    if (typeof obj === 'object') {
      return obj[Object.keys(obj)[0]]['requiredLength'];
    }
  }

  getErrorMessage() {
    return this.error;
  }
  public error: any;
  private formError: any;
}
