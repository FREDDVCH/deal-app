/*
* error key map for translate
*/
const ERROR_MESSAGE_KEYS = {
  'required': 'Поле обезательно для заполнения',
  'minlength': 'Мин. длина поля',
  'maxlength': 'Макс. длина поля',
  'email': 'Введите валидный email',
};

export class ErrorMessage {
  public translateKey: string;
  public value: any;
  private key: string;

  constructor(key: string, value: any = '') {
    this.key = key;
    this.translateKey = ERROR_MESSAGE_KEYS[this.key];
    this.value = value;
  }
}
