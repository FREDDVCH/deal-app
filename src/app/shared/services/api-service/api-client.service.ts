import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders } from '@angular/common/http';
import { catchError, shareReplay } from 'rxjs/internal/operators';
import { ObservableInput, throwError } from 'rxjs';
import { UserService } from '../../../core/services/user.service';

import { ToastrService } from 'ngx-toastr';
import get from 'lodash.get';

@Injectable({
  providedIn: 'root'
})
export class ApiClientService {
  headers: HttpHeaders = new HttpHeaders();

  constructor(private httpClient: HttpClient,
              private userService: UserService,
              private toast: ToastrService) {
    this.userService.authenticated
      .subscribe((status) => {
        if (status) {
          this.headers = new HttpHeaders()
            .set('Authorization', 'Bearer ' + this.userService.getToken())
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        } else {
          this.resetToken();
        }
      });
  }

  /*
 * Reset headers to default
 */
  resetToken() {
    this.headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json');
  }

  /*
  * get data by generic
  * @param {string} path
  * @param {boolean} token
  */
  get<T>(path: string, token: boolean = true) {
    const options = {};
    if (token) {
      Object.assign(options, { headers: this.headers });
    }

    return this.httpClient.get(path, options)
      .pipe(
        shareReplay(),
        catchError((err) => this.errorHandler(err)));
  }

  /*
  * put data by generic
  * @param {string} path
  * @param {T} data
  * @param {boolean} token
  */
  put<T>(path: string, data: T, token: boolean = true) {
    const options = {};
    if (token) {
      Object.assign(options, { headers: this.headers });
    }

    return this.httpClient.put(path, data, options)
      .pipe(
        shareReplay(),
        catchError((err) => this.errorHandler(err)));
  }

  /*
  * post data by generic
  * @param {string} path
  * @param {T} data
  * @param {boolean} token
  */
  post<T>(path: string, data: T, token: boolean = true) {
    const options = {};
    if (token) {
      Object.assign(options, { headers: this.headers });
    }

    return this.httpClient.post(path, data, options)
      .pipe(
        shareReplay(),
        catchError((err) => this.errorHandler(err)));
  }

  /*
  * post data by generic
  * @param {string} path
  * @param {boolean} token
  */
  delete(path: string, token: boolean = true) {
    const options = {};
    if (token) {
      Object.assign(options, { headers: this.headers });
    }

    return this.httpClient.delete(path, options)
      .pipe(
        shareReplay(),
        catchError((err) => this.errorHandler(err)));
  }

  /*
  * error handler
  * @param {HttpErrorResponse} error
  * @return {ObservableInput<any>}
  */
  private errorHandler(error: HttpErrorResponse): ObservableInput<any> {
    const defaultError = 'Server error';
    switch (error.status) {
      case 401:
        this.userService.removeToken();
        this.toast.error(get(error, 'error.message', 'Token is expired'), 'Error!');
        break;
      default:
        this.toast.error(get(error, 'error.message', defaultError), 'Error!');
        break;
    }

    return throwError(get(error, 'message', 'Token is expired'));
  }
}
