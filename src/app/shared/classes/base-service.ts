import { HttpErrorResponse } from '@angular/common/http';
import { ObservableInput, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';

import get from 'lodash.get';

export class BaseService {
  constructor() {}

  protected errorHandler(error: HttpErrorResponse): ObservableInput<any> {
    console.error(error.error || error.message);
    console.log(error.status);

    return throwError(get(error, 'message', 'Error whitout message'));
  }

  protected buildPath(endpoint: string, ...paths) {
    if (paths) {
      paths.push(endpoint);
      endpoint = paths.join('/');
    }

    return `${environment.PATHS.API}/${endpoint}`;
  }

}
