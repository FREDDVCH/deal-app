import { FormBuilder, FormGroup } from '@angular/forms';
import { OnDestroy } from '@angular/core';

import { SubSink } from 'subsink';

export class BaseForm implements OnDestroy {
  form: FormGroup;
  fields: object = {};
  isLoading: boolean;
  subs: SubSink = new SubSink() ;

  constructor(protected fb: FormBuilder) {}

  initForm(fields: object = this.fields) {
    this.form = this.fb.group(fields);
  }

  submit() {
    console.log('/realizate submit');
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
