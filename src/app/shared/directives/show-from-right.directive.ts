import {
  AfterContentInit,
  Directive,
  ElementRef,
  HostListener,
  Input } from '@angular/core';

@Directive({
  selector: '[appShowFromRight]'
})
export class ShowFromRightDirective implements AfterContentInit {

  @Input() appShowFromRight: HTMLElement;
  @Input() minusMarginRight = '-50px';

  @HostListener('mouseenter') onMouseEnter() {
    this.appShowFromRight.style.marginRight = '0';

  }

  @HostListener('mouseleave') onMouseLeave() {
    this.appShowFromRight.style.marginRight = this.minusMarginRight;
  }

  constructor(private element: ElementRef) {

  }

  ngAfterContentInit() {
    this.appShowFromRight.style.marginRight = this.minusMarginRight;
    this.appShowFromRight.style.transition = 'all 0.3s cubic-bezier(.25,.8,.25,1)';
  }

}
