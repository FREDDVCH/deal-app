import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowFromRightDirective } from './directives/show-from-right.directive';
import {
  FormsModule,
  ReactiveFormsModule } from '@angular/forms';
import { DealButtonComponent } from './components/deal-button/deal-button.component';
import { DealInputComponent } from './components/deal-input/deal-input.component';

const components = [
  ShowFromRightDirective,
  DealButtonComponent,
  DealInputComponent
];
const modules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule
];

@NgModule({
  declarations: [...components],
  imports: [...modules],
  exports: [
    ...components,
    ...modules
  ]
})
export class SharedModule { }
