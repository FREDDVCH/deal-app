import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  authenticated = new BehaviorSubject(false);

  private token: string;

  constructor() {
    this.token = localStorage.getItem('token');

    this.authenticated.next(!!this.token);
  }

  /*
  * get token from storage
  * @return {string}
  */
  getToken(): string {
    return this.token;
  }

  /*
  * set token to storage and emit broadcast
  * @param {string} token
  */
  setToken(token: string): void {
    this.token = token;
    localStorage.setItem('token', token);
    this.broadcastLogin();
  }

  /*
  * emit broadcast login
  */
  broadcastLogin(): void {
    this.authenticated.next(true);
  }

  /*
  * emit broadcast logout
  */
  broadcastLogout(): void {
    this.authenticated.next(false);
  }

  /*
  * remove token from storage
  */
  removeToken(): void {
    this.token = null;
    localStorage.removeItem('token');

    this.broadcastLogout();
  }

  /*
  * get current token status
  * @return {boolean}
  */
  isLogged(): boolean {
    return !!this.token;
  }

}
