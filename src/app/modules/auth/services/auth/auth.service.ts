import { Injectable } from '@angular/core';
import { User } from '../../../../shared/models/user';
import { BaseService } from '../../../../shared/classes/base-service';
import { ApiClientService } from '../../../../shared/services/api-service/api-client.service';
import { map } from 'rxjs/internal/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../../../environments/environment';

import get from 'lodash.get';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  constructor(private apiService: ApiClientService) { super(); }

  /*
  * login method
  * @param {User} user
  * @return {Observable<string>}
  */
  login(user: User): Observable<string> {
    return this.apiService.post<User>(this.buildPath(environment.PATHS.ENDPOINTS.LOGIN), user, false)
      .pipe(map((res) => get(res, 'body.token', 'Invalid key!')));
  }

  /*
  * signIn method
  * @param {User} user
  * @return {Observable<string>}
  */
  signIn(user: User): Observable<string> {
    return this.apiService.post<User>(this.buildPath(environment.PATHS.ENDPOINTS.REGISTRATION), user, false)
      .pipe(map((res) =>  get(res, 'body.token', 'Invalid key!')));
  }

}
