import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLoginComponent } from './components/auth-login/auth-login.component';
import { AuthRegistrationComponent } from './components/auth-registration/auth-registration.component';
import { AuthComponent } from './auth.component';

const children: Routes = [
  {
    path: 'login',
    component: AuthLoginComponent
  },
  {
    path: 'registration',
    component: AuthRegistrationComponent
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
