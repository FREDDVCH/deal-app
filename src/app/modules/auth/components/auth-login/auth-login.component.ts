import { Component, OnDestroy } from '@angular/core';
import {
  FormBuilder,
  Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import {
  finalize,
  tap } from 'rxjs/internal/operators';
import { BaseForm } from '../../../../shared/classes/base-form';
import { UserService } from '../../../../core/services/user.service';

@Component({
  selector: 'app-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.scss']
})
export class AuthLoginComponent extends BaseForm implements OnDestroy {
  protected apiMethod: string;

  constructor(protected fb: FormBuilder,
              protected authService: AuthService,
              protected userService: UserService,
              protected router: Router) {
    super(fb);
    this.apiMethod = 'login';
    this.fields = {
      email: ['email@email.com', Validators.compose([Validators.required, Validators.email])],
      password: ['123456', Validators.compose([Validators.required, Validators.minLength(6)])],
    };
    this.initForm(this.fields);
  }

  /*
  * submit form and login
  */
  submit(): void {
   this.isLoading = true;
   this.subs.add(this.authService[this.apiMethod](this.form.value)
     .pipe(
       finalize(() => { this.isLoading = false; }),
       tap((token: string) => this.userService.setToken(token)))
     .subscribe(() => this.responseHandler()));
  }

  /*
  * response handler
  */
  responseHandler(): void {
    this.router.navigate(['/dashboard']);
  }
}
