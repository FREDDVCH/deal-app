import { Component } from '@angular/core';
import {
  FormBuilder,
  Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../../../../core/services/user.service';
import { AuthLoginComponent } from '../auth-login/auth-login.component';

@Component({
  selector: 'app-auth-registration',
  templateUrl: './auth-registration.component.html',
  styleUrls: ['./auth-registration.component.scss']
})
export class AuthRegistrationComponent extends AuthLoginComponent  {

  constructor(protected fb: FormBuilder,
              protected authService: AuthService,
              protected userService: UserService,
              protected router: Router) {
    super(fb, authService, userService, router);
    this.apiMethod = 'signIn';
    Object.assign(this.fields, {username: ['', Validators.required]});
    this.initForm(this.fields);
  }

}
