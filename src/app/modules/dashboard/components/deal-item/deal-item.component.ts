import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output } from '@angular/core';

@Component({
  selector: 'app-deal-item',
  templateUrl: './deal-item.component.html',
  styleUrls: ['./deal-item.component.scss']
})
export class DealItemComponent implements OnInit {

  @Input() editMode: boolean;
  @Output() deleted = new EventEmitter<any>();
  @Output() updated = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

  /*
  * emit delete event
  */
  deleteDeal() {
    this.deleted.next();
  }

  /*
  * emit update event
  */
  updateDeal() {
    this.updated.next();
  }

}
