import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BaseForm } from '../../../../shared/classes/base-form';
import { Deal } from '../../../../shared/models/deal';
import { DealInputComponent } from '../../../../shared/components/deal-input/deal-input.component';

import get from 'lodash.get';

@Component({
  selector: 'app-deal-create',
  templateUrl: './deal-create.component.html',
  styleUrls: ['./deal-create.component.scss']
})
export class DealCreateComponent extends BaseForm implements OnChanges {
  @Input() deal: Deal;

  @Output() unselect = new EventEmitter<boolean>();
  @Output() update = new EventEmitter<Deal>();
  @Output() add = new EventEmitter<Deal>();

  @ViewChild(DealInputComponent, { static: true }) contentElement: DealInputComponent;

  addMode = false;
  editingDeal: Deal;

  constructor(protected fb: FormBuilder) {
    super(fb);
    this.fields = {
      id: [''],
      content: ['', Validators.required]
    };
    this.initForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setFocus();
    if (get(this.deal, 'id', false)) {
      this.editingDeal = { ...this.deal };
      this.form.patchValue(this.editingDeal);
      this.addMode = false;
    } else {
      this.resetForm();
      this.addMode = true;
    }
  }

  /*
  * submit form action
  */
  submit(): void {
    if (!this.form.valid) {
      return;
    }

    if (this.addMode && this.form.valid) {
      this.addDeal();
      this.clear();
    } else if (this.form.valid) {
      this.updateDeal();
      this.clear();
    }
  }

  /*
  * patch form by default object
  */
  resetForm() {
    this.editingDeal = { id: undefined, content: '' };
    this.form.patchValue(this.editingDeal, { emitEvent: false });
  }

  /*
  * clear form and emit unselect
  */
  clear() {
    this.resetForm();
    this.unselect.emit();
  }

  /*
  * set input focus
  */
  setFocus() {
    this.contentElement.setFocus();
  }

  /*
  * emit add event
  */
  addDeal() {
    this.add.emit( this.form.value);
  }

  /*
  * emit update event
  */
  updateDeal() {
    this.update.emit( this.form.value);
  }

}
