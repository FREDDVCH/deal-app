import { Component, OnInit } from '@angular/core';
import { Deal } from '../../shared/models/deal';
import { Observable } from 'rxjs';
import { DealsService } from './services/deals.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  editMode: boolean;
  selectedDeal: Deal;
  deals$: Observable<Deal[]> = new Observable<Deal[]>();

  constructor(private dealsService: DealsService) {
    this.deals$ = dealsService.deals$;
  }

  trackById(deal: Deal) {
    return deal.id;
  }

  ngOnInit() {
    this.getDeals();
  }

  /*
  * get deals
  */
  getDeals(): void {
    this.get();
  }

  /*
  * select item and turn on edit mode
  * @param {Deal} deal
  */
  selectItem(deal: Deal): void {
    this.editMode = true;
    this.selectedDeal = deal;
  }

  /*
  * unselect item and turn off edit mode
  */
  unselectItem(): void {
    this.editMode = false;
    this.selectedDeal = null;
  }

  /*
  * update item
  * @param {Deal} deal
  */
  updateItem(deal: Deal): void {
    this.update(deal);
  }

  /*
  * delete item
  * @param {Deal} deal
  */
  deleteItem(deal: Deal): void {
    this.delete(deal);
  }

  /*
  * add item
  * @param {Deal} deal
  */
  addItem(deal: Deal): void {
    this.add(deal);
  }

  /*
  * update private method
  * @param {Deal} deal
  */
  private update(deal: Deal): void {
    this.dealsService.put(deal);
  }

  /*
  * delete private method
  * @param {Deal} deal
  */
  private delete(deal: Deal): void {
    this.dealsService.delete(deal);
  }

  /*
  * get private method
  */
  private get(): void {
    this.dealsService.get();
  }

  /*
  * add private method
  */
  private add(deal: Deal): void {
    this.dealsService.post(deal);
  }

}
