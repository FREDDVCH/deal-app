import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Deal } from '../../../shared/models/deal';
import { BaseService } from '../../../shared/classes/base-service';
import { ApiClientService } from '../../../shared/services/api-service/api-client.service';
import { environment } from '../../../../environments/environment';
import { catchError, map, tap } from 'rxjs/internal/operators';
import { HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DealsService extends BaseService {
  // Service's  cache
  deals: Deal[] = [];

  // for filtering and refreshed deals list
  deals$: Subject<Deal[]> = new Subject<Deal[]>();

  constructor(private apiService: ApiClientService) { super(); }

  /*
  * Update cached data and push into observable
  * @param {Deal[]} newDeals
  */
  private next(newDeals: Deal[]): void {
    this.deals = newDeals;
    this.deals$.next(newDeals);
  }

  /*
  * Get all data from db, update deals$ observer
  */
  get() {
    this.apiService
      .get<Deal[]>(this.buildPath(environment.PATHS.ENDPOINTS.DEALS))
      .pipe(
        map((response: HttpResponse<Deal[]>) => response.body),
        tap((results) => this.next(results)),
        catchError(this.errorHandler))
      .subscribe();
  }

  /*
  * Post data to db, update deals$ observer
  * @param {Deal} deal
  */
  post(data: Deal) {
    this.apiService
      .post<Deal>(this.buildPath(environment.PATHS.ENDPOINTS.DEALS), data)
      .pipe(
        map((response: HttpResponse<Deal[]>) => response.body),
        tap((result) => this.next(this.deals.concat(result))),
        catchError(this.errorHandler))
      .subscribe();
  }

  /*
  * Update data in db, update deals$ observer
  * @param {Deal} deal
  */
  put(data: Deal) {

    this.apiService
      .put<Deal>(this.buildPath(data.id, environment.PATHS.ENDPOINTS.DEALS), data)
      .pipe(
        map((response: HttpResponse<Deal[]>) => response.body),
        tap((result) => this.next(refreshData(this.deals, result[0]))),
        catchError(this.errorHandler))
      .subscribe();

    function refreshData(arr: Deal[], target: Deal): Deal[] {
      const result: Deal[] = arr.slice();
      const index = result.findIndex(el => el.id === target.id);
      result.findIndex(el => el.id === target.id);
      result[index] = target;

      return result;
    }

  }

  /*
  * Delete data from db, update deals$ observer
  * @param {Deal} deal
  */
  delete(data: Deal) {
    this.apiService
      .delete(this.buildPath(data.id, environment.PATHS.ENDPOINTS.DEALS))
      .pipe(
        map((response: HttpResponse<Deal[]>) => response.body),
        tap((result) => this.next(this.deals.filter(el => !result.find((e) => el.id === e.id)))),
        catchError(this.errorHandler))
      .subscribe();
  }

}
