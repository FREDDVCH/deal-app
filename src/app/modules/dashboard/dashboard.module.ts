import { NgModule } from '@angular/core';
import { DealListComponent } from './components/deal-list/deal-list.component';
import { DealCreateComponent } from './components/deal-create/deal-create.component';
import { DealItemComponent } from './components/deal-item/deal-item.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { DealsService } from './services/deals.service';

@NgModule({
  declarations: [
    DealListComponent,
    DealCreateComponent,
    DealItemComponent,
    DashboardComponent],
  imports: [
    SharedModule,
    DashboardRoutingModule
  ],
  providers: [
    DealsService
  ]
})
export class DashboardModule { }
