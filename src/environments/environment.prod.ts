export const environment = {
  production: true,
  PATHS: {
    API: 'http://localhost:3200/api/v1',
    ENDPOINTS: {
      LOGIN: 'users/login',
      REGISTRATION: 'users/signup',
      DEALS: 'deals',
      ME: 'users/me'
    }
  }
};
