module.exports = (res, data, status, message) => {
    res
        .status(status)
        .json({
            status,
            success: true,
            message: message ? message : 'Request successfully',
            body: data ? data : []
        });
};



