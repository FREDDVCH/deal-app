const express = require('express');
const controller = require('../controllers/auth');
const router = express.Router();

router.post('/login', controller.login);
router.post('/signup', controller.signup);
router.post('/me', controller.checkHealth);
module.exports = router;
