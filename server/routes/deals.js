const express = require('express');
const dealController = require('../controllers/deal');
const passport = require('passport');
const router = express.Router();

router.get('/', passport.authenticate('jwt', { session: false }), dealController.getAll);
router.put('/:dealId', passport.authenticate('jwt', { session: false }), dealController.put);
router.post('/', passport.authenticate('jwt', { session: false }), dealController.create);
router.delete('/:dealId', passport.authenticate('jwt', { session: false }), dealController.delete);

module.exports = router;
