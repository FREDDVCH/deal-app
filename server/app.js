const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');

const authRoutes = require('./routes/auth');
const dealsRoutes = require('./routes/deals');

const bodyParser = require('body-parser');
const { api, db } = require('./config/dev');
const loggerService = require('./utils/loggerService');
const app = express();
const path = require(`path`);

mongoose.connect(db.uri,
  { useNewUrlParser: true,
    useFindAndModify: false})
.then(() => console.log(`MondoDB connected ...`))
.catch(err => console.log(err));

app.use(loggerService);

process.on(`uncaughtException`, (reason) => {
  console.log(`Unhandled exception reason:`, reason);
});

process.on(`unhandledRejection`, (reason, p) => {
  console.log(`Unhandled Rejection at: Promise`, p, `reason:`, reason);
});

app.use(passport.initialize());
require('./middleware/passport')(passport);

app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(bodyParser.json());

app.use(api.auth, authRoutes);
app.use(api.deals, dealsRoutes);

const DIST_FOLDER = path.join(__dirname, '..', `dist/deal-app`);
app.use(express.static(DIST_FOLDER));
app.get('*', (req, res) => res.sendFile(path.resolve(DIST_FOLDER, 'index.html')));

module.exports = app;
