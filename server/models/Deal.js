const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const deal = new Schema({
  content: {
    type: String,
    required: true
  },
  user: {
    ref: 'users',
    type: Schema.Types.ObjectId
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

deal.method('toClient', function() {
  const obj = this.toObject();

  //Rename fields
  obj.id = obj._id;
  delete obj._id;
  delete obj.__v;
  delete obj.user;

  return obj;
});

module.exports = mongoose.model('deal', deal);
