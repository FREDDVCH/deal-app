const apiEndpoint = `/api/v1`;

module.exports = {
  api: {
    auth: `${apiEndpoint}/users`,
    deals: `${apiEndpoint}/deals`,
  },
  db: {
    user: `user1`,
    password: `user123`,
    uri: `mongodb://user1:user123@ds137605.mlab.com:37605/deal-app`
  },
  jwtKey: `dev-jwt`
};
