"use strict";

const Deal = require('../models/Deal');
const errorHandler = require('../handlers/errorHandler');
const responseHandler = require('../handlers/responseHandler');

module.exports.getAll = async (req, res) => {
  try {
    const deals = await Deal.find({ user: req.user._id });
    responseHandler(res, deals.map(el => el.toClient()), 200);
  } catch (err) {
    errorHandler(res, err);
  }
};

module.exports.create = async (req, res) => {
  const deal = new Deal({
    user: req.user._id,
    content: req.body.content
  });
  try {
    await deal.save();
    responseHandler(res, [ deal.toClient() ], 201, 'Create succesfully');
  } catch (err) {
    errorHandler(res, err);
  }
};

module.exports.put = async (req, res) => {
  const options = { _id: req.params.dealId };

  try {
    let deal = await Deal.findOneAndUpdate(options, req.body, {new: true});

    if (!deal) {
      throw new Error(`not exist`);
    }

    responseHandler(res, [ deal.toClient() ], 201, 'Create succesfully');
  } catch (err) {
    errorHandler(res, err);
  }
};

module.exports.delete = async (req, res) => {
  const options = { _id: req.params.dealId };
  try {
    const deal = await Deal.findOne(options);

    if (!deal) {
      throw new Error(`not exist`);
    }

    await Deal.remove(options);
    responseHandler(res, [deal.toClient()], 201);
  } catch (err) {
    errorHandler(res, err);
  }
};
