const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const { jwtKey } = require('../config/dev');
const errorHandler = require('../handlers/errorHandler');
const responseHandler = require('../handlers/responseHandler');

module.exports.login = async (req, res) => {
    const candidate = await User.findOne({ email: req.body.email });
    if (candidate) {
        // check the password, the user is exist
        const passwordResult = bcrypt.compareSync(req.body.password, candidate.password);
        if (passwordResult) {
            // generate a token
            const token = jwt.sign({
                userId: candidate._id,
                email: candidate.email,
                username: candidate.username,
            }, jwtKey , { expiresIn: 60*60 });

            responseHandler(res, {
              username: candidate.username,
              email: candidate.email,
              token: `${token}`
            }, 201, 'Login successfully');

        } else {
            // send a password incorrect error
            res
                .status(401)
                .json({
                    message: 'Password incorrect'
                })
        }
    } else {
        // user is not exist
        res
            .status(404)
            .json({
                message: 'The user not found'
            });
    }
};

module.exports.signup = async (req, res) => {
    // check exist user
    const candidate = await User.findOne({ email: req.body.email });

    if (candidate) {
        res
            .status(409)
            .json({
               message: 'the user whit this email already exist'
            });
    } else {
        // registration a new user;
        // hash password before user save;
        const salt = bcrypt.genSaltSync(10);
        const password = req.body.password;
        console.log(req.body);
        const user = new User({
            username: req.body.username,
            email: req.body.email,
            password: bcrypt.hashSync(password, salt)
        });

        try {
            await user.save();
            // redirect user to home page
            //res.set('Location', `http://localhost:4200/home`);
            const token = jwt.sign({
            userId: user._id,
            email: user.email,
            username: user.username,
          }, jwtKey , { expiresIn: 60*60 });

          responseHandler(res, {
            username: user.username,
            email: user.email,
            token: `${token}`
          }, 201, 'User created succesfully');
        } catch(err) {
            errorHandler(res, err);
        }

    }

};

module.exports.checkHealth = async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.user._id});
    responseHandler(res, user, 201, 'User fetched succesfully');
  } catch(err) {
    errorHandler(res, err);
  }
};
