const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const mongoose = require('mongoose');
const User = mongoose.model('user');
const { jwtKey } = require('../config/dev');

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey:  jwtKey
};

module.exports = passport => {
  passport.use(
      new JwtStrategy(options, async (payload, done) => {
          try {
              const user = await User.findById(payload.userId).select('email id');

              if (user) {
                  done(null, user);
              } else {
                  done(null, false);
              }
          } catch (err) {
           console.log('passport error: ', err);
          }
      })
  );
};