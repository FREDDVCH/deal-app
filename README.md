# DealApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.4.
This app recommended Node version 10.16.2

## Build and start project

install all dependencies in root folder npm install

install all dependencies in server folder npm install

Run `ng build --prod --aot` to build the project. 

and after success build start `npm run server`

server runs on port 3200  

visited localhost:3200

