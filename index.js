const app = require('./server/app');
const PORT = process.env.PORT || 3200;

app.listen(PORT, () => {
    console.log(`server has been started on port: ${PORT}`);
});
